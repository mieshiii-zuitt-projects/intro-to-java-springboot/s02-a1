package com.delacruz.b142.s02.s02app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S02appApplication {

	public static void main(String[] args) {
		SpringApplication.run(S02appApplication.class, args);
	}

}
