package com.delacruz.b142.s02.s02app.models;

import javax.persistence.*;

@Entity
@Table(name="users")
public class User {

   // Properties (columns)
    @Id
    @GeneratedValue
    private Long id; //primary key
    @Column
    private String username;
    @Column
    private String password;

    // Constructors
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    // Getters and Setters
    // Getters
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    // Setter
    public void setUsername(String newUsername) {
        this.username = newUsername;
    }

    public void setPassword(String newPassword) {
        this.password = newPassword;
    }

    // Methods

}
